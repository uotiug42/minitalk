# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/11/15 21:35:49 by gbrunet           #+#    #+#              #
#    Updated: 2024/01/04 11:54:35 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minitalk

CLIENT = client

SERVER = server

CC = cc

CFLAGS = -Wall -Wextra -Werror

LIBFT = libft

INCLUDES = includes

SRC_DIR = sources/

OBJ_DIR = objects/

SRC_FILES_CLIENT = client utils 

SRC_FILES_SERVER = server utils

SRC_CLIENT = $(addprefix $(SRC_DIR), $(addsuffix .c, $(SRC_FILES_CLIENT)))

SRC_SERVER = $(addprefix $(SRC_DIR), $(addsuffix .c, $(SRC_FILES_SERVER)))

OBJ_CLIENT = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC_FILES_CLIENT)))

OBJ_SERVER = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC_FILES_SERVER)))

SRC_CLIENT_BONUS = $(addprefix $(SRC_DIR), \
				   $(addsuffix _bonus.c, $(SRC_FILES_CLIENT)))

SRC_SERVER_BONUS = $(addprefix $(SRC_DIR), \
				   $(addsuffix _bonus.c, $(SRC_FILES_SERVER)))

OBJ_CLIENT_BONUS = $(addprefix $(OBJ_DIR), \
				   $(addsuffix _bonus.o, $(SRC_FILES_CLIENT)))

OBJ_SERVER_BONUS = $(addprefix $(OBJ_DIR), \
				   $(addsuffix _bonus.o, $(SRC_FILES_SERVER)))

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@
	
.PHONY : all clean fclean re norme bonus

all : $(NAME)

lib :
	make -C $(LIBFT)

$(NAME) : $(CLIENT) $(SERVER)

$(CLIENT) : lib $(OBJ_CLIENT)
	$(CC) $(CFLAGS) $(OBJ_CLIENT) -o $(CLIENT) -lft -L ./libft

$(SERVER) : lib $(OBJ_SERVER)
	$(CC) $(CFLAGS) $(OBJ_SERVER) -o $(SERVER) -lft -L ./libft

clean :
	make clean -C $(LIBFT)
	$(RM) -rf $(OBJ_DIR)

fclean : clean
	make fclean -C $(LIBFT)
	$(RM) $(CLIENT)
	$(RM) $(SERVER)

re : fclean all

bonus : lib $(OBJ_SERVER_BONUS) $(OBJ_CLIENT_BONUS);
	$(CC) $(CFLAGS) $(OBJ_SERVER_BONUS) -o $(SERVER) -lft -L ./libft
	$(CC) $(CFLAGS) $(OBJ_CLIENT_BONUS) -o $(CLIENT) -lft -L ./libft

norme :
	@norminette $(SRC_CLIENT) $(LIBFT) $(SRC_SERVER)\
		$(SRC_CLIENT_BONUS) $(SRC_SERVER_BONUS) | grep -v Norme -B1 || true
	@norminette $(INCLUDES) -R CheckDefine | grep -v Norme -B1 || true

