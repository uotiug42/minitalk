/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 18:12:43 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/04 12:18:35 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minitalk.h"

int	g_can_send_signal;

int	is_pid(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!ft_isdigit(str[i]))
			return (0);
	return (1);
}

void	send_char(char c, int pid)
{
	int	i;

	i = 0;
	while (i++ < 8)
	{
		g_can_send_signal = 0;
		if (c & 1)
			send_signal(SIGUSR2, pid);
		else
			send_signal(SIGUSR1, pid);
		c = c >> 1;
		while (!g_can_send_signal)
			usleep(10);
	}
}

void	server_signal(int signum)
{
	if (signum == SIGUSR1)
		g_can_send_signal = 1;
}

int	error(void)
{
	ft_putstr_fd("Error.\nTo use this programme, type :\n", 2);
	ft_putstr_fd("./client <server_PID> \"message to send\"", 2);
	return (0);
}

int	main(int ac, char **av)
{
	int	i;
	int	pid;

	if (ac != 3)
		return (error());
	if (!is_pid(av[1]))
		return (error());
	signal(SIGUSR1, &server_signal);
	signal(SIGUSR2, &server_signal);
	pid = ft_atoi(av[1]);
	i = -1;
	while (av[2][++i])
		send_char(av[2][i], pid);
	send_char(0, pid);
}
