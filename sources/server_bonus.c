/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_bonus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 17:49:00 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/05 15:40:49 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minitalk.h"

void	end_message(char c, int pid)
{
	if (c == 0)
	{
		ft_printf("\n\033[0m");
		if (kill(pid, SIGUSR2) < 0)
			ft_printf("\033[0;31mUnable to reach the client...\033[0m\n");
	}
}

void	update_msg(char **msg, char c)
{
	char	*temp;
	int		i;

	temp = ft_strdup(*msg);
	free (*msg);
	*msg = malloc(sizeof(char) * (ft_strlen(temp) + 2));
	if (!*msg)
	{
		ft_printf("Something went wrong...\n");
		exit(0);
	}
	i = -1;
	while (temp[++i])
		(*msg)[i] = temp[i];
	(*msg)[i] = c;
	(*msg)[i + 1] = 0;
	free(temp);
	if (c == 0)
	{
		ft_printf("%s\n", *msg);
		free(*msg);
		*msg = NULL;
	}
}

void	init_msg(char **msg, int *pid, siginfo_t *info)
{
	if (!*msg)
	{
		free(*msg);
		*msg = NULL;
	}
	*msg = malloc(sizeof(char) * 1);
	if (!*msg)
	{
		ft_printf("Something went wrong...\n");
		exit(0);
	}
	(*msg)[0] = 0;
	*pid = info->si_pid;
	ft_printf("\033[0;35;1mMessage from %d :\n\033[0m\033[0;32m", *pid);
}

void	handler(int signum, siginfo_t *info, void *unused)
{
	static char	c = 0;
	static int	b = 0;
	static int	pid = 0;
	static char	*msg = NULL;

	(void)unused;
	if (pid == 0 || pid != info->si_pid)
		init_msg(&msg, &pid, info);
	if (signum == SIGUSR1)
		b++;
	else if (signum == SIGUSR2)
	{
		c |= 1 << b;
		b++;
	}
	if (b == 8)
	{
		update_msg(&msg, c);
		end_message(c, info->si_pid);
		c = 0;
		b = 0;
	}
	usleep(10);
	kill(info->si_pid, SIGUSR1);
}

int	main(void)
{
	struct sigaction	sa_usr1;
	struct sigaction	sa_usr2;

	sa_usr1.sa_flags = SA_SIGINFO | SA_RESTART | SA_NODEFER;
	sigemptyset(&sa_usr1.sa_mask);
	sa_usr1.sa_sigaction = handler;
	sa_usr2.sa_flags = SA_SIGINFO | SA_RESTART | SA_NODEFER;
	sigemptyset(&sa_usr2.sa_mask);
	sa_usr2.sa_sigaction = handler;
	if (sigaction(SIGUSR1, &sa_usr1, NULL) == -1)
	{
		ft_printf("\033[0;31mSomething went wrong...\033[0m\n");
		exit(1);
	}
	if (sigaction(SIGUSR2, &sa_usr2, NULL) == -1)
	{
		ft_printf("\033[0;31mSomething went wrong...\033[0m\n");
		exit(1);
	}
	ft_printf("PID : \033[0;30;43;1m %d \033[0m\n", getpid());
	while (1)
		pause();
	return (0);
}
