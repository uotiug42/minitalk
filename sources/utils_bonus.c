/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/04 11:52:43 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/04 11:55:27 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minitalk.h"

void	send_signal(int signum, int pid)
{
	if (kill(pid, signum) < 0)
	{
		ft_printf("\033[0;31mUnable to reach the server...\033[0m\n");
		exit(1);
	}
}
