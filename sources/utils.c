/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/04 11:52:43 by gbrunet           #+#    #+#             */
/*   Updated: 2024/01/04 12:17:04 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minitalk.h"

void	send_signal(int signum, int pid)
{
	if (kill(pid, signum) < 0)
	{
		ft_printf("Unable to reach the server...\n");
		exit(1);
	}
}
